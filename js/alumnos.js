
let alumnos = [
    {
        "matricula":"20203022",
        "nombre":"Osuna Echeagaray José antonio de Jesús",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/5.png"
    },
    {
        "matricula":"20203020",
        "nombre":"Arias Tirado Mateo",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/2.png"
    },
    {
        "matricula":"20203021",
        "nombre":"Qui Mora Angel Ernesto",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/1.png"
    },
    {
        "matricula":"20203023",
        "nombre":"Quezada Lara Julio Emiliano",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/3.png"
    },
    {
        "matricula":"20203024",
        "nombre":"Yohan Alek Plazuela",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/4.png"
    },
    {
        "matricula":"20203025",
        "nombre":"Tirado Rios Luis Mario",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/6.png"
    },
    {
        "matricula":"20203026",
        "nombre":"Ontiveros Govea Yahir",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/7.png"
    },
    {
        "matricula":"20203027",
        "nombre":"Tirado Rios Oscar",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/8.png"
    },
    {
        "matricula":"20203028",
        "nombre":"Solis Velarde Oscar",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/9.png"
    },
    {
        "matricula":"20203029",
        "nombre":"Tirado Romero Jhonatan",
        "grupo":"TI-73",
        "carrera":"Técnologias de la Información",
        "foto":"/img/alumnos/10.png"
    }
];

function generarTabla(alumnos) {
    let tabla = "<table border='1'>";
    
    // Encabezados
    tabla += "<thead>";
    tabla += '<tr class="headers">';
    for (let key in alumnos[0]) {
        tabla += `<th>${key}</th>`;
    }
    tabla += "</tr>";
    tabla += "</thead>";

    // Datos
    tabla += "<tbody>";
    for (let obj of alumnos) {
        tabla += '<tr class="alumno" draggable="true">';
        for (let key in obj) {
            if (key === "foto") {
                // Si el campo es 'imagen', genera una etiqueta <img>
                tabla += `<td data-label="Foto"><img src="${obj[key]}" alt="Imagen de ${obj.nombre}" width="100"></td>`;
            } else if(key === "matricula"){
                tabla += `<td data-label="Matricula">${obj[key]}</td>`;
            }else if(key === "nombre"){
                tabla += `<td data-label="Nombre">${obj[key]}</td>`;
            }else if(key === "grupo"){
                tabla += `<td data-label="Grupo">${obj[key]}</td>`;
            }else{
                tabla += `<td data-label="Carrera">${obj[key]}</td>`;
            }
        }
        tabla += "</tr>";
    }
    tabla += "</tbody>";

    tabla += "</table>";

    return tabla;
}



//Hacer las filas ordenables

let draggedItem = null;

document.addEventListener('dragstart', function(e) {
    if (e.target.classList.contains('alumno')) {
        draggedItem = e.target;
        setTimeout(function() {
            draggedItem.style.opacity = '0.5';
        }, 0);
    }
});

document.addEventListener('dragend', function(e) {
    setTimeout(function() {
        if (draggedItem) {
            draggedItem.style.opacity = '';
            draggedItem = null;
        }
    }, 0);
});

document.addEventListener('dragover', function(e) {
    e.preventDefault();
});

document.addEventListener('drop', function(e) {
    e.preventDefault();
    if (e.target.closest('.alumno')) {
        let targetRow = e.target.closest('.alumno');
        let targetParent = targetRow.parentNode;
        let targetIndex = Array.from(targetParent.children).indexOf(targetRow);
        let draggedIndex = Array.from(targetParent.children).indexOf(draggedItem);
        if (draggedIndex < targetIndex) {
            targetParent.insertBefore(draggedItem, targetRow.nextSibling);
        } else {
            targetParent.insertBefore(draggedItem, targetRow);
        }
    }
});

window.addEventListener('DOMContentLoaded', (event) => {
    // Espera 1 segundo después de que el DOM esté completamente cargado
    setTimeout(function() {
        document.getElementById("table-container").innerHTML = generarTabla(alumnos);
        const alumnosElements = document.querySelectorAll('.alumno');
        alumnosElements.forEach((alumno, index) => {
            // Establece un retraso de animación basado en el índice del alumno
            alumno.style.animationDelay = `${index * 150}ms`;
            alumno.style.opacity = '0';  // Asegúrate de que esté oculto antes de comenzar la animación
        });
    }, 1000);
});





/* console.log("Matricula: " + alumno.matricula);
console.log("Nomre: " +alumno.nombre);
console.log("Grupo: " +alumno.grupo);
console.log("Carrera: " +alumno.carrera);

// Creando un objeto compuesto
let persona = {
    nombre: "Carlos",
    edad: 32,
    direccion: {
        calle: "Avenida Central",
        numero: 123,
        ciudad: "San José",
        pais: "Costa Rica",
        codigoPostal: "1000"
    },
    hobbies: ["leer", "correr", "cocinar"],
    familia: {
        conyuge: {
            nombre: "Luisa",
            edad: 29
        },
        hijos: [
            {
                nombre: "Miguel",
                edad: 5
            },
            {
                nombre: "Ana",
                edad: 3
            }
        ]
    }
};

// Accediendo a los valores del objeto compuesto
console.log(persona.nombre);             // Muestra "Carlos"
console.log(persona.direccion.calle);    // Muestra "Avenida Central"
console.log(persona.hobbies[1]);         // Muestra "correr"
console.log(persona.familia.hijos[0].nombre); // Muestra "Miguel"

// arreglo de productos

let productos = [
    {
        "SKU":"1000",
        "descripcion": "Audifonos",
        "precio":"1500"},
    {
        "SKU":"1001",
        "descripcion": "Mouse Gamer",
        "precio": "1000"},
    {
        "SKU":"1002",
        "descripcion": "Monitor 27 Pulgadas",
        "precio": "6000"},
    {
        "SKU":"1003",
        "descripcion": "Teclado Gamer",
        "precio": "2000"}
    ];

//Mostrar todos los objetos y sus atributos del arreglo

 for(let producto of productos) {
    console.log("SKU: " + producto.SKU + ", Descripción: " + producto.descripcion + ", Precio: " + producto.precio);
 } */

