function establecerImagenFondo(imc) {
    let imagePath = '/img/';
    if (imc < 18.5) {
        imagePath += '01.png';
    } else if (imc >= 18.5 && imc <= 24.9) {
        imagePath += '02.png';
    } else if (imc >= 25 && imc <= 29.9) {
        imagePath += '03.png';
    } else if (imc >= 30 && imc <= 34.9) {
        imagePath += '04.png';
    } else if (imc >= 35 && imc <= 39.9) {
        imagePath += '05.png';
    } else {
        imagePath += '06.png';
    }
    
    // Establecer la imagen de fondo para el section con clase imc-img
    document.querySelector('.imc-img').style.backgroundImage = `url(${imagePath})`;
}

function mostrarValores(edad, peso) {
    let genero = document.querySelector('input[name="sexo"]:checked').value;
    let tabla = '<table border="1">';
    if (genero === "hombre") {
        tabla += '<thead><tr><th>Edad</th><th>Hombre</th></tr></thead><tbody>';
    } else {
        tabla += '<thead><tr><th>Edad</th><th>Mujer</th></tr></thead><tbody>';
    }

    let valorCalorias;
    if (edad >= 10 && edad < 18) {
        tabla += `<tr><td>${edad}</td>`;
        if (genero === "hombre") {
            valorCalorias = (17.686 * peso + 658.2).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        } else {
            valorCalorias = (13.384 * peso + 692.6).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        }
        tabla += '</tr>';
    } else if (edad >= 18 && edad < 30) {
        tabla += `<tr><td>${edad}</td>`;
        if (genero === "hombre") {
            valorCalorias = (15.057 * peso + 692.2).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        } else {
            valorCalorias = (14.818 * peso + 486.6).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        }
        tabla += '</tr>';
    } else if (edad >= 30 && edad < 60) {
        tabla += `<tr><td>${edad}</td>`;
        if (genero === "hombre") {
            valorCalorias = (11.472 * peso + 873.1).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        } else {
            valorCalorias = (8.126 * peso + 845.6).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        }
        tabla += '</tr>';
    } else if (edad >= 60) {
        tabla += `<tr><td>${edad}</td>`;
        if (genero === "hombre") {
            valorCalorias = (11.711 * peso + 587.7).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        } else {
            valorCalorias = (9.082 * peso + 658.5).toFixed(2);
            tabla += `<td>${valorCalorias}</td>`;
        }
        tabla += '</tr>';
    }
    tabla += '</tbody></table>';
    document.getElementById('tablaValores').innerHTML = tabla;
}

document.getElementById('btnCalcularImc').addEventListener('click', function() {
    // Obtener los valores ingresados por el usuario
    let peso = parseFloat(document.getElementById('peso').value) || 0;
    let alturaCm = parseFloat(document.getElementById('altura').value) || 0;
    let edad = parseInt(document.getElementById('edad').value) || 0;
    
    // Convertir la altura a metros
    let altura = alturaCm;
    
    // Verificar que la altura y el peso sean válidos
    if(altura !== 0 && peso !== 0) {
        // Calcular el IMC
        let imc = peso / (altura * altura);
        
        // Actualizar los campos de resultado en el formulario
        document.getElementById('imcResultado').value = imc.toFixed(2); // Redondear a dos decimales
        establecerImagenFondo(imc);
        mostrarValores(edad, peso);
    } else {
        // Mostrar un mensaje de error si la altura o el peso son inválidos
        alert('Por favor ingresa valores válidos para el peso y la altura');
    }
});

