
const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function() {
    // Obtener los datos de los inputs
    let valorAuto = parseFloat(document.getElementById('ValorAuto').value) || 0;
    let pInicial = parseFloat(document.getElementById('porcentaje').value) || 0;
    let plazos = parseInt(document.getElementById('plazos').value) || 0;

    // Hacer los cálculos
    let pagoInicial = valorAuto * (pInicial / 100);
    let totalFin = valorAuto - pagoInicial;
    let pagoMensual = totalFin / plazos;

    // Mostrar los datos
    document.getElementById('pagoInicial').value = pagoInicial.toFixed(2);
    document.getElementById('totalfin').value = totalFin.toFixed(2);
    document.getElementById('pagoMensual').value = pagoMensual.toFixed(2);
    
    // Listar el historial de valores
    let registro = document.querySelector('#historial tbody');

    // Crear una nueva fila de tabla para el registro actual
    let nuevaFila = document.createElement('tr');
    nuevaFila.innerHTML = `
      <td>$${pagoInicial.toFixed(2)}</td>
      <td>$${totalFin.toFixed(2)}</td>
      <td>$${pagoMensual.toFixed(2)}</td>
    `;

    // Agregar la nueva fila al historial
    registro.appendChild(nuevaFila);
});


function limpiarForm(){
    document.getElementById('pagoInicial').value = '';
    document.getElementById('totalfin').value = '';
    document.getElementById('pagoMensual').value = '';
    document.getElementById('ValorAuto').value = '';
    document.getElementById('porcentaje').value = '';
    document.getElementById('plazos').value = '';
}

/* Manejo de arrays*/

//declaracion de arrays con elementos enteros

let arreglo = [4,89,30,10,34,89,10,5,8,28];
//funcion que resive arreglo de enteros como argumento e imprime cada elemento y el tamaño

function mostrarArray(arreglo){
  let tamaño = arreglo.length;

  for( let con=0 ; con < arreglo.length; ++ con){
    console.log(con + ":" , arreglo[con]);
  }
  console.log("tamaño; "+ tamaño);
}

//funcion para mostrar promedio

console.log(calcularPromedio(arreglo))

function calcularPromedio(arreglo){
  let sum = 0;
  for(let i=0; i < arreglo.length; ++i){
    sum += arreglo[i];
    
  }
  console.log("Promedio:" + sum/arreglo.length);
}


//funcion para mostarr valores pares
console.log(mostrarPares(arreglo));
function mostrarPares(arreglo){
  return arreglo.filter(num => num % 2 === 0);
}


//mostrar el elemento mayor

console.log(eMayor(arreglo));

function eMayor(arreglo) {
  let mayor = arreglo[0];
  for (let i = 1; i < arreglo.length; i++) {
    if (arreglo[i] > mayor) {
      mayor = arreglo[i];  // Actualizar la variable mayor si se encuentra un elemento mayor
    }
  }
  console.log("El elemento mayor es:", mayor);
  return mayor;
}

//funcion para llenar arreglo con valores aleatorios

console.log(numRandom(arreglo));
function numRandom(arreglo){
  let tamaño = arreglo.length;
  
  for(let i =0 ; i<tamaño; i++){
    arreglo[i] = Math.floor(Math.random() * 10);
    console.log("Posicion: " +i +"valor" + arreglo[i]);
  }
}

//funcion que muestra el valor menor y la posicion del arreglo

console.log(eMenor(arreglo));

function eMenor(arreglo) {
  let menor = arreglo[0];
  for (let i = 1; i < arreglo.length; i++) {
    if (arreglo[i] < menor) {
      menor = arreglo[i];  // Actualizar la variable mayor si se encuentra un elemento mayor
    }
  }
  console.log("El elemento menor es:", menor);
  return menor;
}

//funcion para comprobar si el generador de numeros aleatorios 
//es simetrico es decir que la diferencia entre numeros pares e impares no sea mayor al 20%

let cantidad = 10;
verificarSimetria(cantidad);
function verificarSimetria(cantidad) {
  let pares = 0;
  let impares = 0;
  for (let i = 0; i < cantidad; i++) {
    let numero = Math.floor(Math.random() * 10);
    if (numero % 2 === 0) {
      pares++;
    } else {
      impares++;
    }
    console.log("Valor: "+numero)
  }

  let proporcionPares = pares / cantidad;
  let proporcionImpares = impares / cantidad;

  let diferencia = Math.abs(proporcionPares - proporcionImpares) * 10;
  if (diferencia > 20) {
    console.log('El generador no es simétrico, diferencia:', diferencia.toFixed(2) + '%');
    return false;
  } else {
    console.log('El generador es simétrico, diferencia:', diferencia.toFixed(2) + '%');
    return true;
  }
}

