document.addEventListener('DOMContentLoaded', (event) => {
    document.querySelector('input[type="button"]').addEventListener('click', generarNumeros);
    document.getElementById('btnLimpiar').addEventListener('click', limpiarValores);
});

function generarNumeros() {
    const cantidad = parseInt(document.getElementById('cantidadGenerar').value);
    if (isNaN(cantidad) || cantidad <= 0) {
        alert('Por favor, ingresa una cantidad válida de números a generar.');
        return;
    }

    const arreglo = [];
    for (let i = 0; i < cantidad; i++) {
        arreglo.push(Math.floor(Math.random() * 100));
    }

    llenarSelect(arreglo);
    mostrarMayorMenor(arreglo);
    mostrarPares(arreglo);
    mostrarPromedio(arreglo);
    mostrarSimetria(arreglo);
}

function llenarSelect(arreglo) {
    const select = document.getElementById('arrayNumeros');
    select.innerHTML = '';
    arreglo.forEach(num => {
        const option = document.createElement('option');
        option.value = num;
        option.text = num;
        select.add(option);
    });
}

function mostrarMayorMenor(arreglo) {
    document.getElementById('numMayor').textContent = Math.max(...arreglo);
    document.getElementById('numMayorPo').textContent = arreglo.indexOf(Math.max(...arreglo));
    document.getElementById('numMenor').textContent = Math.min(...arreglo);
    document.getElementById('numMenorPo').textContent = arreglo.indexOf(Math.min(...arreglo));
}

function mostrarPares(arreglo) {
    document.getElementById('pares').textContent = arreglo.filter(num => num % 2 === 0).join(', ');
}

function mostrarPromedio(arreglo) {
    let suma = 0;
    for (let i = 0; i < arreglo.length; i++) {
        suma += arreglo[i];
    }
    const promedio = suma / arreglo.length;
    document.getElementById('promedio').textContent = promedio.toFixed(2);
}

function mostrarSimetria(arreglo) {
    const paresCount = arreglo.filter(num => num % 2 === 0).length;
    const porcentajePares = (paresCount / arreglo.length) * 100;
    const porcentajeImpares = 100 - porcentajePares;
    document.getElementById('prcntPar').textContent = porcentajePares.toFixed(2) + '%';
    document.getElementById('prcntImpar').textContent = porcentajeImpares.toFixed(2) + '%';
    
    const diferenciaPorcentaje = Math.abs(porcentajePares - porcentajeImpares);
    document.getElementById('resultSim').textContent = diferenciaPorcentaje <= 20 ? 'El arreglo es simétrico' : 'El arreglo no es simétrico';
}

function limpiarValores() {
    document.getElementById('cantidadGenerar').value = '';
    document.getElementById('arrayNumeros').innerHTML = '<option value="Vacio">Vacio</option>';
    document.getElementById('numMayor').textContent = '';
    document.getElementById('numMayorPo').textContent = '';
    document.getElementById('numMenor').textContent = '';
    document.getElementById('numMenorPo').textContent = '';
    document.getElementById('pares').textContent = '';
    document.getElementById('promedio').textContent = '';
    document.getElementById('prcntPar').textContent = '';
    document.getElementById('prcntImpar').textContent = '';
    document.getElementById('resultSim').textContent = '';
}
